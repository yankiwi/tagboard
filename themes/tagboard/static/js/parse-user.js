function login_user() {
    Parse.initialize(parseAppID, parseJSKey);
    Parse.serverURL = parseServerURL;

    var username = document.getElementById("inputUser").value;
    var password = document.getElementById("inputPassword").value;

    Parse.User.logIn(username, password).then((userdata) => {
        if (typeof document !== 'undefined') {
            console.log('Logged in user', userdata);
            window.location = baseURL;
        }
      }).catch(error => {
        if (typeof document !== 'undefined') {
            console.error('Error while logging in user', error);
            // FIXME - need error element
        }
      });
    return false;
}


function logged_in() {
    Parse.initialize(parseAppID, parseJSKey);
    Parse.serverURL = parseServerURL;

    var userData = Parse.User.current()

    if (userData != null) {
        console.log('Current user', userData.id);
        return [true, userData.id];
    } else {
        return [false, null]
    }
}


function nav_current_user() {
    Parse.initialize(parseAppID, parseJSKey);
    Parse.serverURL = parseServerURL;

    currentUser = logged_in();
    if (currentUser[0]) { // user is logged in
        var userTbl = Parse.Object.extend('User');
        var query = new Parse.Query(userTbl);
        query.get(currentUser[1]).then((userDataMore) => {
            document.getElementById("nav_username").innerHTML = '<span class="icon ion-md-person"></span>' + userDataMore.attributes.username;
            document.getElementById("nav_login").innerHTML = '<span class="icon ion-md-log-out"></span>' + " Logout";
        }, (error) => { // user is logged in but username query failed
          console.error('Error while fetching User object', error);
          document.getElementById("nav_username").innerHTML = '<span class="icon ion-md-person"></span>' + "Guest";
          document.getElementById("nav_login").innerHTML = '<span class="icon ion-md-log-out"></span>' + " Login";
        });
    } else { // user is logged out
        document.getElementById("nav_username").innerHTML = '<span class="icon ion-md-person"></span>' + "Guest";
        document.getElementById("nav_login").innerHTML = '<span class="icon ion-md-log-out"></span>' + " Login"
    }
}


function nav_login_logout() {
    Parse.initialize(parseAppID, parseJSKey);
    Parse.serverURL = parseServerURL;

    var currentUser = logged_in();
    if (currentUser[0]) { // user logged in, logout clicked
        Parse.User.logOut().then(() => {
            var currentUser = logged_in();
            if (currentUser[0]) {
                console.error('Logout failed');
            } else {
                document.getElementById("nav_username").innerHTML = '<span class="icon ion-md-person"></span>' + "Guest";
                document.getElementById("nav_login").innerHTML = '<span class="icon ion-md-log-out"></span>' + " Login";
            }
        });
    } else { // user logged out, redirect to login page
        window.location = baseURL + "page/login/";
    }

}
