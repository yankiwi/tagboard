var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 20000
};
    
var maplat = -41.838875;
var maplong = 171.7799;
var mapzoom = 5;

function success(pos) {
  var crd = pos.coords;

  console.log('Your current position is:');
  maplat  = crd.latitude;
  console.log(`Latitude : ${maplat}`);
  maplong = crd.longitude;
  console.log(`Longitude: ${maplong}`);
  console.log(`More or less ${crd.accuracy} meters.`);
  mapzoom = 8;
}
    
function error(err) {
  console.warn(`ERROR(${err.code}): ${err.message}`);
}

navigator.geolocation.getCurrentPosition(success, error, options);