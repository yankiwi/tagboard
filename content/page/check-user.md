---
title: Check User
subtitle: TAGboard
comments: false
---

<link href="/css/signin.css" rel="stylesheet">
<form class="form-signin" onsubmit="nav_current_user()">
  <h1 class="h3 mb-3 font-weight-normal">Check Current User</h1>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Check</button>
</form>
