---
title: About
subtitle: TAGboard
comments: false
---

Test site for [TAGboard](https://yankiwi.gitlab.io/tagboard/).

## JAMstack:

- Gitlab Pages
- Hugo
- Back4App (Parse Server)

### JS Libraries

- Parse
- Bootstrap 4
- VisJS
- JQuery