---
title: Login
subtitle: TAGboard
comments: false
---

<link href="/css/signin.css" rel="stylesheet">
<form class="form-signin" onsubmit="return false;">
  <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
  <label for="inputUser" class="sr-only">Username</label>
  <input type="text" id="inputUser" class="form-control" placeholder="Username" required="" autofocus="">
  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">
  <button class="btn btn-lg btn-primary btn-block" type="submit" onclick="login_user();">Sign in</button>
</form>
